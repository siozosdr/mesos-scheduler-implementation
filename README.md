# README #

This repository contains the Apache Mesos Scheduler implementation which was part of my internship in JourneyXP in collaboration with another coworker. It is developed in Java 1.8, using the Apache Mesos library for Java.

### What is this repository for? ###

This repository is used for Java development using Mesos. The current state of the repo is about implementing a CSV Uploader by utilizing the Mesos/Marathon infrastructure. In addition a basic Round Robin Scheduling implementation has been added to the scheduler to evenly distribute jobs to the Mesos agents.
The main class of the project is "./src/jxp2/Demo.java".

### Contribution guidelines ###
TODO
 - Add Weighted Round Robin as a different scheduling method.
 - Test for different cases

### Who do I talk to? ###
ssd@journeyxp.com/siozosdr@gmail.com - Repo Owner/Developer
tha@journeyxp.com - Repo Owner/Developer