package jxp2;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.TimeoutException;

import org.apache.mesos.Scheduler;
import org.apache.mesos.SchedulerDriver;
import org.apache.mesos.Protos.ExecutorID;
import org.apache.mesos.Protos.FrameworkID;
import org.apache.mesos.Protos.MasterInfo;
import org.apache.mesos.Protos.Offer;
import org.apache.mesos.Protos.OfferID;
import org.apache.mesos.Protos.Resource;
import org.apache.mesos.Protos.SlaveID;
import org.apache.mesos.Protos.TaskInfo;
import org.apache.mesos.Protos.TaskStatus;

import utils.RoundRobinScheduler;
import utils.WRRScheduler;

/**
 * @author Thanos
 * Mesos Scheduler Interface
 */
public class DemoScheduler implements Scheduler {
	
	private Queue<Job> jobs = new LinkedList<Job>();
	
	//Pack many tasks in a single offer (first fit)
	public List<TaskInfo> packTasks(Offer offer) {
		
		List<TaskInfo> toLaunch = new ArrayList<>();
		double offerCpus = 0;
		double offerMem = 0;
		
		for (Resource r : offer.getResourcesList()) {
			if(r.getName().equals("cpus")) {
				offerCpus += r.getScalar().getValue();
			}
			else if(r.getName().equals("mem")) {
				offerMem += r.getScalar().getValue();
			}
		}	
		while(!jobs.isEmpty() && offerCpus >= jobs.element().getCpus() && offerMem >= jobs.element().getMem()) {
			offerCpus -= jobs.element().getCpus();
			offerMem -= jobs.element().getMem();
			Job j = jobs.remove();
			String command = j.getCommand();
			TaskInfo taskinfo = j.makeTask(offer.getSlaveId(), command);
			toLaunch.add(taskinfo);
		}			
		return toLaunch;
	}
			
	///////////////////////////////////////////////////////////////
	/////////////////////	MESOS EVENTS	///////////////////////
	///////////////////////////////////////////////////////////////
	
	@Override
	public void registered(SchedulerDriver driver, FrameworkID frameworkId, MasterInfo masterInfo) {
	
	System.out.println("Registered with framework id " + frameworkId);	
	}
	
	@Override
	public void reregistered(SchedulerDriver driver, MasterInfo masterInfo) {
	// TODO Auto-generated method stub
	
	}

	@Override
	public void resourceOffers(SchedulerDriver driver, List<Offer> offers) {
		try {
			RmqMonitor rmqMonitor = new RmqMonitor();
			rmqMonitor.monitor();
			RoundRobinScheduler rrSched = new RoundRobinScheduler(rmqMonitor.getJobs());
			// need to test WRR scheduling
			//WRRScheduler wrrSched = new WRRScheduler(rmqMonitor.getJobs());
			jobs.addAll(rrSched.getJobs());
		} catch (IOException e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TimeoutException e) {
		// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (Offer offer : offers) {
			if(jobs.isEmpty()) {
				driver.declineOffer(offer.getId());
				break;
			}
			driver.launchTasks(
			Collections.singletonList(offer.getId()),
			packTasks(offer)
			);
		}		
	}
	
	@Override
	public void offerRescinded(SchedulerDriver driver, OfferID offerId) {
	// TODO Auto-generated method stub
	
	}
	
	@Override
	public void statusUpdate(SchedulerDriver driver, TaskStatus status) {
	System.out.println("Got status update "+ status.getState());			
	}
	
	@Override
	public void frameworkMessage(SchedulerDriver driver,
	ExecutorID executorId, SlaveID slaveId, byte[] data) {
	// TODO Auto-generated method stub
	
	}
	
	@Override
	public void disconnected(SchedulerDriver driver) {
	// TODO Auto-generated method stub
	
	}
	
	@Override
	public void slaveLost(SchedulerDriver driver, SlaveID slaveId) {
	// TODO Auto-generated method stub
	
	}
	
	@Override
	public void executorLost(SchedulerDriver driver, ExecutorID executorId,
	SlaveID slaveId, int status) {
	// TODO Auto-generated method stub
	
	}
	
	@Override
	public void error(SchedulerDriver driver, String message) {
	// TODO Auto-generated method stub
	
	}
}