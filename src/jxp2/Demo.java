package jxp2;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.mesos.MesosSchedulerDriver;
import org.apache.mesos.Scheduler;
import org.apache.mesos.SchedulerDriver;
import org.apache.mesos.Protos.FrameworkInfo;

/**
 * @author Thanos
 * JourneyXP Mesos demo launcher
 * Reads path to Mesos from configuration file and registers framework with mesos
*/
public class Demo {
	public static void main(String[] args) {
		
		Properties prop = new Properties();
		String mesosPath;
	    try(InputStream input = new FileInputStream("/opt/config.properties")){
	        prop.load(input);
	        mesosPath = prop.getProperty("mesosPath");
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	        return;
	    } catch (IOException e) {
	        e.printStackTrace();
	        return;
	    }
		//Register framework with Mesos
		FrameworkInfo frameworkInfo = FrameworkInfo.newBuilder()
				.setUser("")//register with mesos as whichever user is running the scheduler
				.setName("Validation Pipeline")
				.build();
		Scheduler mySched = new DemoScheduler();
		//TODO Remove hardcoded path to mesos
		SchedulerDriver driver = new MesosSchedulerDriver(
				mySched,
				frameworkInfo,
				mesosPath);
		driver.start();
		driver.join();
	}
}