package jxp2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import org.apache.mesos.Protos.CommandInfo;
import org.apache.mesos.Protos.Resource;
import org.apache.mesos.Protos.SlaveID;
import org.apache.mesos.Protos.TaskID;
import org.apache.mesos.Protos.TaskInfo;
import org.apache.mesos.Protos.Value;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author Thanos
 * Job Class
 * Loads task properties from configuration file
 * Prepares task for launch
 */
public class Job {
		private double cpus;
		private double mem;
		private String command = "java -jar";
		private String[] commandArgs;
		private String dbUsername;
		private String dbPassword;
		private String csvProcessor;
		private int fileID;
		private int priority;
		private int batchsize;
		/**
		 * Constructor
		 * @param JSONObject j
		 * @throws JSONException
		 */
		public Job(JSONObject j) throws JSONException {
			// load properties
			read();
			// initialize job object
			fileID = j.getInt("fileid");
			priority = j.getInt("priority");
			batchsize = j.getInt("batchsize");
			commandArgs = new String[] {
										csvProcessor,
										j.getString("url"),
										j.getString("table"),
										j.getString("progressTable"),
										dbUsername,
										dbPassword,
										j.getString("filename"),
										String.valueOf(j.getInt("fileid")),
										String.valueOf(j.getInt("checkpoint")),
										String.valueOf(j.getInt("totalcount")),
										String.valueOf(j.getInt("batchsize"))
										};
			for(int i=0; i< commandArgs.length; ++i){
				command += " " + commandArgs[i];
			}	
		}
		
		/**
		 * Read properties file
		 */
		public void read(){
			Properties prop = new Properties();
		    try(InputStream input = new FileInputStream("/opt/config.properties")){
		        prop.load(input);
		        cpus = Double.parseDouble(prop.getProperty("cpus"));
		        mem = Double.parseDouble(prop.getProperty("mem"));
		        dbUsername = prop.getProperty("mySQLdbusername");
		        dbPassword = prop.getProperty("mySQLdbpassword");
		        csvProcessor = prop.getProperty("csvProcessor");
		    } catch (FileNotFoundException e) {
		        e.printStackTrace();
		    } catch (IOException e) {
		        e.printStackTrace();
		    }
		}
		
		/**
		 * Prepares task for launch
		 * @param targetSlave
		 * @param command
		 * @return TaskInfo
		 */
		public TaskInfo makeTask(SlaveID targetSlave, String command) {

			UUID uuid = UUID.randomUUID();
			TaskID id = TaskID.newBuilder()
					.setValue(uuid.toString())
					.build();
			return TaskInfo.newBuilder()
					.setName("Customer batch") 
					.setTaskId(id)
					.addResources(Resource.newBuilder()
							.setName("cpus")
							.setType(Value.Type.SCALAR)
							.setScalar(Value.Scalar.newBuilder().setValue(cpus)))
					.addResources(Resource.newBuilder()
							.setName("mem")
							.setType(Value.Type.SCALAR)
							.setScalar(Value.Scalar.newBuilder().setValue(mem)))
					.setCommand(CommandInfo.newBuilder().setValue(command))
					.setSlaveId(targetSlave)
					.build();
		}
		
		public double getCpus() {
			return cpus;
		}
		
		public double getMem() {
			return mem;
		}
		
		public String getCommand() {
			return command;
		}
		
		public int getFileID() {
			return fileID;
		}

		public int getPriority(){return priority;}

		public int getBatchsize(){return batchsize;}
	}