package jxp2;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @author Thanos
 * RabbitMQ monitor
 * Depends on RmqConsumer, Job
 */
public class RmqMonitor {
	
	private Queue<Job> jobs = null;
	private String QUEUE_NAME = null ;
	private String rmqUsername = null;
	private String rmqPassword = null;
	private String rmqHostIp = null;
	
	//Constructor
	public RmqMonitor() {
		// Read arguments from property file
		jobs = new LinkedList<>();
		read();
	}
	
	/**
	 * Read properties file
	 */
	public void read(){
		Properties prop = new Properties();
	    try(InputStream input = new FileInputStream("/opt/config.properties")){
	        prop.load(input);
	        QUEUE_NAME = prop.getProperty("publishQueue");
	        rmqUsername = prop.getProperty("rmqUsername");
	        rmqPassword = prop.getProperty("rmqPassword");
	        rmqHostIp = prop.getProperty("rmqAddress");
	    } catch (FileNotFoundException e) {
	        e.printStackTrace();
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
	
	/**
	 * Connects to RabbitMQ, checks for messages and consumes them
	 * @throws IOException
	 * @throws TimeoutException
	 */
	public void monitor() throws IOException, TimeoutException{		
		//Connect with rabbitmq
		ConnectionFactory factory = new ConnectionFactory();
		factory.setUsername(rmqUsername);
        factory.setPassword(rmqPassword);
        factory.setVirtualHost("/");
		factory.setHost(rmqHostIp);
	 	factory.setPort(5671);
        try {
            factory.useSslProtocol("TLSv1.2");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            System.out.println("SSL Exception");
            e.printStackTrace();
        }
		Connection connection = factory.newConnection();
		Channel monitorChannel = connection.createChannel();
		
		//get number of messages and consume them
		int msgs = monitorChannel.queueDeclarePassive(QUEUE_NAME).getMessageCount();
		if(msgs>0){
			System.out.println(" [*] RabbitMQ Broker messages --> " + msgs);	
			Channel consumerChannel = connection.createChannel();
			RmqConsumer consumer = new RmqConsumer(consumerChannel, QUEUE_NAME);
			for(int i =0; i<msgs; ++i){
				consumer.rmqConsume(consumerChannel);
			}
			jobs = consumer.getJobs();
			
		}
		monitorChannel.close();
		connection.close();
	}
	
	public Queue<Job> getJobs(){
		return jobs;
	}
}
