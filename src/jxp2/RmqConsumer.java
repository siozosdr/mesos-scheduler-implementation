package jxp2;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.TimeoutException;

import org.json.JSONException;
import org.json.JSONObject;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.GetResponse;

/**
 * @author Thanos
 * RabbitMQ consumer
 * Depends on Job.java
 */
class RmqConsumer extends DefaultConsumer {
			
	private Queue<Job> jobs = new LinkedList<>();
	private String QUEUE_NAME = null;
	
	/**
	 * Constructor
	 * @param channel
	 * @param queue_name
	 */
	public RmqConsumer(Channel channel, String queue_name) {
		super(channel);
		QUEUE_NAME = queue_name;
	}
	
	/**
	 * Returns Jobs to be launched
	 * @return Jobs List
	 */
	public Queue<Job> getJobs() {
		return jobs;
	}
	
	/**
	 * Prepares message for consuming
	 */
	public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
			throws IOException {
		//get and parse message body
		String message = new String(body, "UTF-8");
		System.out.println(" [x] Received '" + message + "'");

		try {
			JSONObject j = new JSONObject(message);
			jobs.add(new Job(j));					
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Task added!");
	}
	
	/**
	 * Consumes RabbitMQ messages
	 * @param consumerchannel
	 * @throws IOException
	 * @throws TimeoutException
	 */
	public void rmqConsume(Channel consumerchannel) throws IOException, TimeoutException {
		//connect with RabbitMQ server
		consumerchannel.queueDeclare(QUEUE_NAME, false, false, false, null);
		
		GetResponse response = consumerchannel.basicGet(QUEUE_NAME, false); // get one message without auto-acknowledge delivery
		handleDelivery(getConsumerTag(), response.getEnvelope(), response.getProps(), response.getBody()); //consume message
		consumerchannel.basicAck(response.getEnvelope().getDeliveryTag(), false);//send message delivery-acknowledgment
		System.out.println(" [*] Message delivered, ack-delivery completed");
	}				
}
