package utils;

import jxp2.Job;

import java.util.*;

public class WRRScheduler {
    // Data structures - TODO
    HashMap<Integer, FileQueue> files = null;
    double smallestNormalizedWeight;
    Queue <Job> jobsToBeServed = new LinkedList<>();
    public WRRScheduler(Queue<Job> jobs){
        while(!jobs.isEmpty()){
            Job j = jobs.poll();
            FileQueue f = files.get(j.getFileID());
            // If file queue already exists add the job to the Queue
            if(f != null){
                f.addJob(j);
                files.put(j.getFileID(), f);
            } else{ // If job is added for the first time calculate required weights
                double weight = j.getBatchsize() * j.getPriority();
                // mean batch size and standard are the same for our case, depending on the arguments of SFTPMonitor
                f = new FileQueue(j.getPriority(), weight, j.getBatchsize(), j.getBatchsize());
                f.setNormalizedWeight(weight/j.getBatchsize());
                files.put(j.getFileID(), f);
            }

        }
        prepareData();
        schedule();
        serveJobs();
    }
    // Functions - TODO
    public void schedule(){
        Iterator it = files.entrySet().iterator();
        while(!files.isEmpty()){
            while(it.hasNext()) {
                Map.Entry<Integer, FileQueue> pair = (Map.Entry<Integer,FileQueue>)it.next();
                FileQueue f = pair.getValue();
                // if f is not empty do the bellow loop
                if(!f.isEmpty()){
                    int limit = Math.min(f.getPacketsToBeServed(), f.getPacketsWaiting());
                    for(int i=0; i < limit; i++){
                        jobsToBeServed.add(f.pop());
                    }
                }else {// else remove f from files
                    it.remove();
                }
            }
        }

    }

    // TODO
    public Queue<Job> serveJobs(){
        return jobsToBeServed;
    }
    // TODO
    public void prepareData(){
        smallestNormalizedWeight = findSmallestNormalizedWeight();
        Iterator it = files.entrySet().iterator();
        while(it.hasNext()) {
            Map.Entry<Integer, FileQueue> pair = (Map.Entry<Integer,FileQueue>)it.next();
            FileQueue f = pair.getValue();
            f.setPacketsToBeServed((int)Math.ceil(f.getNormalizedWeight()/smallestNormalizedWeight));
            f.setPacketsWaiting(f.getSize());
        }
    }
    // TODO
    private double findSmallestNormalizedWeight(){
        Iterator it = files.entrySet().iterator();
        double min = Double.MIN_VALUE;
        while(it.hasNext()) {
            Map.Entry<Integer, FileQueue> pair = (Map.Entry<Integer,FileQueue>)it.next();
            FileQueue f = pair.getValue();
            if(f.getNormalizedWeight() < min){
                min = f.getNormalizedWeight();
            }
        }
        return min;
    }
}
