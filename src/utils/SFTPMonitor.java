package utils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.nio.file.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeoutException;

/**
 * @author Sokratis
 */
public class SFTPMonitor {
    public static void main(String[] args) {
        // Read arguments from property file
        Properties prop = new Properties();
        InputStream input = null;
        ArrayList<String> propArgs = new ArrayList();
        try{
            input = new FileInputStream("config.properties");
            prop.load(input);
            propArgs.add(prop.getProperty("rmqAddress"));
            propArgs.add(prop.getProperty("rmqUsername"));
            propArgs.add(prop.getProperty("rmqPassword"));
            propArgs.add(prop.getProperty("directory"));
            propArgs.add(prop.getProperty("publishQueue"));
            propArgs.add(prop.getProperty("batchSize"));
            propArgs.add(prop.getProperty("mySQLAddress"));
            propArgs.add(prop.getProperty("mySQLDB"));
            propArgs.add(prop.getProperty("mySQLTable"));
            propArgs.add(prop.getProperty("mySQLProgressTable"));
            propArgs.add(prop.getProperty("mySQLDBUsername"));
            propArgs.add(prop.getProperty("mySQLDBPassword"));
            propArgs.add(prop.getProperty(("priority")));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(propArgs.size()!=13){
            System.out.println("Properties file not configured correctly. Make sure all fields are present");
            System.out.println("rmqAddress=value\n" + "rmqUsername=value\n" + "rmqPassword=value\n" +
                    "directory=value\n" + "publishQueue=value\n" + "batchSize=value\n" +
                    "mySQLAddress=value\n" + "mySQLDB=value\n" + "mySQLTable=value\n" +
                    "mySQLProgressTable=value\n" + "mySQLdbusername=value\n" + "mySQLdbpassword=value\n" +
                    "priority");
        } else{
            SFTPMonitor mon = new SFTPMonitor(
                    propArgs.get(0), propArgs.get(1), propArgs.get(2), propArgs.get(3),
                    propArgs.get(4), Integer.parseInt(propArgs.get(5)), propArgs.get(6), propArgs.get(7),
                    propArgs.get(8), propArgs.get(9), propArgs.get(10), propArgs.get(11), propArgs.get(12));
            try {
                mon.monitor();
            } catch (IOException e) {
                System.out.println("Cannot monitor directory: " + e);
                e.printStackTrace();
            } catch (TimeoutException e) {
                System.out.println("Connection to directory timed out: " + e);
                e.printStackTrace();
            }
        }
    }

    private String directory = null;
    private String publishQueue = null;
    private int batchsize;
    private String separator = null;
    private String rmqAddress = null;
    private String rmqUsername = null;
    private String rmqPassword = null;
    private String mySQLAddress = null;
    private String mySQLDB = null;
    private String mySQLTable = null;
    private String mySQLProgressTable = null;
    private String mySQLdbusername = null;
    private String mySQLdbpassword = null;
    private String priority;
    /**
     * Constructor
     * @param rmqURL
     * @param rmqUsername
     * @param rmqPassword
     * @param directory
     * @param publishQueue
     * @param batchsize
     * @param mySQLAddress
     * @param mySQLDB
     * @param mySQLTable
     * @param mySQLProgressTable
     * @param mySQLdbusername
     * @param mySQLdbpassword
     */
    public SFTPMonitor(String rmqURL, String rmqUsername, String rmqPassword, String directory,
                       String publishQueue, int batchsize, String mySQLAddress, String mySQLDB,
                       String mySQLTable, String mySQLProgressTable, String mySQLdbusername, String mySQLdbpassword,
                       String priority){
        this.mySQLProgressTable = mySQLProgressTable;
        this.mySQLdbusername = mySQLdbusername;
        this.mySQLdbpassword = mySQLdbpassword;
        this.mySQLTable = mySQLTable;
        this.mySQLDB = mySQLDB;
        this.mySQLAddress = mySQLAddress;
        this.rmqPassword = rmqPassword;
        this.rmqUsername = rmqUsername;
        this.rmqAddress = rmqURL;
        this.directory = directory;
        this.publishQueue = publishQueue;
        this.batchsize = batchsize;
        this.priority = priority;
        if(System.getProperty("os.name").contains("Windows")){
            separator = "\\";
        } else {
            separator = "/";
        }
    }
    /**
     * Count number of lines in a file
     * @param filename
     * @param batchsize
     * @return count
     * @throws IOException
     */
    private int countLines(String filename, int batchsize){
        InputStream is = null;
        int count = 0;
        boolean empty = true;
        try {
            is = new BufferedInputStream(new FileInputStream(filename));
            byte[] c = new byte[1024];
            int readChars = 0;
            while ((readChars = is.read(c)) != -1) {
                empty = false;
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        ++count;
                    }
                }
            }
            is.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("File close error: " + e.toString());
            e.printStackTrace();
        }
        return (count == 0 && !empty) ? 1 : count;
    }

    /**
     * Generate message to send to RabbitMQ given file name and number of lines
     * @param filename
     * @param linecount
     * @param checkpoint
     * @param batchsize
     * @param fileID
     * @return json
     * @throws JSONException
     */
    private JSONObject generateMessage(String filename, int linecount, int checkpoint, int batchsize, int fileID) throws JSONException {
//        , "t1","progress","ssd", "jxp",
//                "C:\\Users\\ssd\\IdeaProjects\\CSVChannelUploader\\data\\f2.csv", 0, 1000, 0, 200
        JSONObject json = new JSONObject();
        json.put("filename", filename);
        json.put("totalcount", linecount);
        json.put("checkpoint", checkpoint);
        json.put("url", "jdbc:mysql://"+mySQLAddress+"/"+mySQLDB);
        json.put("table", mySQLTable);
        json.put("progressTable", mySQLProgressTable);
        json.put("fileid", fileID);
        json.put("dbusername", mySQLdbusername);
        json.put("dbpassword", mySQLdbpassword);
        json.put("batchsize", batchsize);
        json.put("priority", priority);
        return json;
    }

    /**Gets filename prefix
     * @param filename
     * @return filename prefix
     */
    private String getCustomerTable(String filename){
        String[] parts = filename.split(".csv");
        System.out.println(parts.length);
        return parts[0];
    }

    /**
     * Monitor directory and signal RabbitMQ when a new file arrives to that directory
     * new messages are sent to the queue named "QUEUE_NAME".
     * TODO: Check rabbitmq ip before execution
     * TODO: remove hardocoded info (vhost)
     * @throws IOException
     * @throws TimeoutException
     */
    public void monitor() throws IOException, TimeoutException {
        //define a folder root
        Path myDir = Paths.get(directory);
        // Connect to RabbitMQ
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername(rmqUsername);
        factory.setPassword(rmqPassword);
        factory.setVirtualHost("/");
        factory.setHost(rmqAddress);
        factory.setPort(5671);
        try {
            factory.useSslProtocol("TLSv1.2");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            System.out.println("SSL Exception");
            e.printStackTrace();
        }
        Connection connection = null;
        Channel channel = null;
        connection = factory.newConnection();
        channel = connection.createChannel();
        channel.queueDeclare(publishQueue, false, false, false, null);
        //channel.queueDeclarePassive(publishQueue);
        // Monitor directory for events
        for (; ; ) {
            try {
                WatchService watcher = myDir.getFileSystem().newWatchService();
                myDir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE,
                        StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);

                WatchKey watchKey = watcher.take();
                // Iterate over list of events
                List<WatchEvent<?>> events = watchKey.pollEvents();
                // directory separator depending on OS java is running

                for (WatchEvent event : events) {
                    // Send message to RabbitMQ in case of new non-empty file
                    if (event.kind() == StandardWatchEventKinds.ENTRY_CREATE) {
                        // absolute path to file
                        String filename = directory + separator + event.context().toString();
                        mySQLTable = getCustomerTable(event.context().toString());
                        int count = countLines(filename, batchsize);
                        if (count != 0) {
                            // generate message for batches
                            int fileID = ThreadLocalRandom.current().nextInt(0, 999999999 + 1);
                            for(int i=0; i< count ; i= i+batchsize){

                                String message = generateMessage(filename, count, i, batchsize, fileID).toString();
                                channel.basicPublish("", publishQueue, null, message.getBytes());
                                System.out.println("Message sent: " + message);
                            }

                        } else {
                            System.out.println("Empty file used");
                        }


                    }
                    if (event.kind() == StandardWatchEventKinds.ENTRY_DELETE) {
                        System.out.println("Delete: " + event.context().toString());
                    }
                    if (event.kind() == StandardWatchEventKinds.ENTRY_MODIFY) {
                        System.out.println("Modify: " + event.context().toString());
//                        channel.basicPublish("", QUEUE_NAME1,null, event.context().toString().getBytes());
                    }
                }
                watchKey.reset();
            } catch (Exception e) {
                assert channel != null;
                //channel.close();
                connection.close();
                System.out.println("Error: " + e.toString());
            }
        }


    }
}

