package utils;

import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class CSVGenerator {
    private ArrayList<String> names = new ArrayList<>();
    private ArrayList<String> surnames = new ArrayList<>();
    private ArrayList<String> emails = new ArrayList<>();
    private ArrayList<String> sex = new ArrayList<>();
    private ArrayList<String> ipaddresses = new ArrayList<>();
    private int total=0;

    /**
     *
     * @param filename file to read samples from
     */
    private void accumulate_data(String filename){
        File file = new File(filename);
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while((line=reader.readLine())!= null && line.length()!=0) {
                String[] parts = line.split(",");
                names.add(parts[1]);
                surnames.add(parts[2]);
                emails.add(parts[3]);
                sex.add(parts[4]);
                ipaddresses.add(parts[5]);
                total+=1;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param output_file file to write generated data to
     * @param size number of lines for the generated CSV
     */
    public void generate_csv(String output_file, int size){
        int current = 1;
        try(PrintWriter writer = new PrintWriter(output_file, "UTF-8")){
            while(current<=size){
                int pick = ThreadLocalRandom.current().nextInt(0, total);
                String line = String.valueOf(current) + "," + names.get(pick) + ","+
                        surnames.get(pick) + "," + emails.get(pick) + "," +
                        sex.get(pick) + "," + ipaddresses.get(pick);
                writer.println(line);
                current+=1;
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
    public static void main(String args[]) {
        CSVGenerator gen = new CSVGenerator();
        gen.accumulate_data("./data_samples/f2.csv");
        gen.generate_csv("./data_samples/newfile5.csv", 10000);
        System.out.println("hello");
    }
}
