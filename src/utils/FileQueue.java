package utils;

// https://docs.oracle.com/javase/7/docs/api/java/util/Queue.html
import jxp2.Job;

import java.util.LinkedList;
import java.util.Queue;

public class FileQueue {
    private int priority = 1;
    private double weight;
    private double standardBatchSize;
    private double meanPacketSize;
    private double normalizedWeight;
    private int packetsToBeServed = 0;
    private int packetsWaiting = 0;
    private Queue<Job> jobs = null;

    public FileQueue(int priority, double weight, double standardBatchSize, double meanPacketSize) {
        jobs = new LinkedList<>();
        this.meanPacketSize = meanPacketSize;
        this.standardBatchSize = standardBatchSize;
        this.weight = weight;
    }

    // TODO
    public void setNormalizedWeight(double normWeight){
        normalizedWeight = normWeight;
    }
    // TODO
    public void setPacketsToBeServed(int packets){
        packetsToBeServed = packets;
    }
    // TODO
    public void setPacketsWaiting(int packets){
        packetsWaiting = packets;
    }

    public int getPacketsToBeServed(){return packetsToBeServed;}

    public int getPacketsWaiting(){return packetsWaiting;}
    public void addJob(Job j) {
        jobs.add(j);
    }

    public double getNormalizedWeight(){
        return normalizedWeight;
    }

    public int getSize(){
        return jobs.size();
    }
    public Job pop(){
        return jobs.poll();
    }
    public boolean isEmpty(){
        return jobs.isEmpty();
    }
}
