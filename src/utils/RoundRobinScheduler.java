package utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Queue;

import jxp2.Job;

public class RoundRobinScheduler {
	
	private Queue<Job> jobs = null;
	private HashMap<Integer, Queue<Job>> files = new HashMap<Integer, Queue<Job>>();
	
	//Constructor
	public RoundRobinScheduler(Queue<Job> jobs) {
		this.jobs = jobs;
		populate();
		scheduleJobs();
	}
	
	private void populate() throws IllegalStateException, ClassCastException, NullPointerException, IllegalArgumentException {
		// while there are jobs to be performed
		while(!jobs.isEmpty()) {
			// remove job from queue
			Job j = jobs.remove();
			// check fileID if exists in hashtable
			if(files.containsKey(j.getFileID())){
				// add job to existing queue
				Queue<Job> temp = files.get(j.getFileID());
				temp.add(j);
				files.put(j.getFileID(), temp);
			}else {
				// else create new queue with the job
				Queue<Job> temp = new LinkedList<Job>();
				temp.add(j);
				files.put(j.getFileID(), temp);
			}
		}
	}
	
	private void scheduleJobs() throws NoSuchElementException {
		// while hashtable has keys
		while(!files.isEmpty()) {
			Iterator it = files.entrySet().iterator();
			//iterate map entries
			while(it.hasNext()) {
				Map.Entry<Integer, Queue<Job>> pair = (Map.Entry<Integer, Queue<Job>>)it.next();
				// remove job from queue and add to Jobs
				jobs.add(pair.getValue().remove());
				// if queue is empty remove file(entry)
				if(pair.getValue().isEmpty()){
					it.remove();
				}
			}
		}
	}
	
	public Queue<Job> getJobs() {
		return jobs;
	}
}
