package utils;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by ssd on 29/06/2017.
 */
public class CSVProcessor {

    private List<List<String>> arrays = null;
    private String url = null;
    private String table = null;
    private String progressTable = null;
    private String username = null;
    private String password = null;
    private String filename = null;
    private int checkpoint;
    private int totalCount;
    private int fileID;
    private int batchsize;
    
    public static void main(String[] args){
        if(args.length!=10){
            System.out.println("Incorrect arguments given");
            System.out.println("REQUIRED Args:");
            System.out.println("url, table, progressTable, dbusername, dbPassword, filename, fileID, checkpoint, totalCount, batchSize");
        }else {
            CSVProcessor p = new CSVProcessor(args[0], args[1],args[2], args[3], args[4], args[5], Integer.valueOf(args[6]), Integer.valueOf(args[7]), Integer.valueOf(args[8]), Integer.valueOf(args[9]));
            p.processData();
            p.storeData();
            p.updateProgress();
        }
    }

    public CSVProcessor(String url, String table, String progressTable, String name, String password, String filename, int fileID, int checkpoint, int totalCount, int batchsize){
        arrays = new ArrayList<>();
        this.url = url;
        this.table = table;
        this.progressTable = progressTable;
        username = name;
        this.password = password;
        this.filename = filename;
        this.checkpoint = checkpoint;
        this.fileID = fileID;
        this.totalCount = totalCount;
        this.batchsize = batchsize;
    }

    /**
     * Check if a table exists in the database
     * @param conn
     * @param tableName
     * @return tExists
     * @throws SQLException
     */
    private boolean tableExist(Connection conn, String tableName) throws SQLException {
        boolean tExists = false;
        try (ResultSet rs = conn.getMetaData().getTables(null, null, tableName, null)) {
            while (rs.next()) {
                String tName = rs.getString("TABLE_NAME");
                if (tName != null && tName.equals(tableName)) {
                    tExists = true;
                    break;
                }
            }
        }
        return tExists;
    }
    /**
     *
     * Opens file with name "filename" and processes part of it to be saved to MySQL later.
     */
    public void processData(){
        // Open file to read from checkpoint
        int currentLine = 0;
        currentLine++;
        File file = new File(filename);
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String text = null;
            for(currentLine = 0; currentLine < checkpoint+batchsize ; currentLine++){
                text = reader.readLine();
                // if checkpoint is reached, start processing operations
                if(currentLine >= checkpoint && currentLine < checkpoint + batchsize){
                    if(text!=null){
                        List<String> textFields = Arrays.asList(text.split(","));
                        List<String> temp = new ArrayList<>();
                        for(int i=0;i<textFields.size();i++){
                            temp.add(textFields.get(i));
                        }
                        // save to class private variable "arrays" / other processing operations
                        arrays.add(temp);
//                        arrays.add(new String[] {textFields.get(0), textFields.get(1), textFields.get(2), textFields.get(3), textFields.get(4), textFields.get(5)});
                    }

                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Function used in case new data cannot be inserted to the table.
     * A new table is created with the name of the filename to store the data to it.
     */
    public void createTable(Connection conn, String table){
        System.out.println("Connecting database...");
        Statement stmt = null;
        // table schema can also be used to create a new table
        String query = "CREATE TABLE " + table +" (\n" +
                "  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,\n" +
                "  `first_name` varchar(40) DEFAULT NULL,\n" +
                "  `last_name` varchar(40) DEFAULT NULL,\n" +
                "  `email` varchar(60) DEFAULT NULL,\n" +
                "  `gender` varchar(10) DEFAULT NULL,\n" +
                "  `ip_address` varchar(20) DEFAULT NULL,\n" +
                "  PRIMARY KEY (`id`)\n" +
                ");";
        try {
            conn.setAutoCommit(false);
            System.out.println("Database connected!");
            stmt = conn.createStatement();
            stmt.executeUpdate(query) ;
            System.out.println("New Table created.");
            conn.commit();
        } catch (SQLException e) {
            // Added exception handling for debugging purposes
            System.err.println("Could not create table!");
            System.err.println("Processor for entries: " +String.valueOf(checkpoint) +"-"+String.valueOf(checkpoint+batchsize));
            System.err.println(String.valueOf(e));
            System.err.println("-----End of Error output-----");

        }
    }
    /**
     * TODO: Use dynamic columns based on sample CSV file
     * Stores batch of the CSV uploaded to the MySQL DB
     * @throws SQLException
     */
    public void storeData() {
        System.out.println("Connecting database...");
        String query = null;
        String errorQuery = null;
        PreparedStatement stmt = null;
        // Put connection only inside the try/catch scope
        try ( Connection connection = DriverManager.getConnection(url, username, password)){
            connection.setAutoCommit(false);
            System.out.println("Database connected!");
            if(!tableExist(connection,table)){
                createTable(connection, table);
            }
            // test file fields: id,first_name,last_name,email,gender,ip_address
            query = "INSERT INTO `"+table+"`" +
                    "(`id`,`first_name`,`last_name`,`email`,`gender`,`ip_address`)" +
                    "VALUES (?, ?, ?, ?, ?, ?);";
//            query = "INSERT INTO `"+table+"`" +
//                    "(`id`,`first_name`,`last_name`,`email`,`gender`,`ip_address`)" +
//                    "VALUES (?, ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE " +
//                   "`first_name`=VALUES(`first_name`), `last_name`=VALUES(`last_name`), `email`=VALUES(`email`)," +
//                   "`gender`=VALUES(`gender`), `ip_address` = VALUES(`ip_address`);";
            stmt = connection.prepareStatement(query);
            for(int i=0; i<arrays.size();i++){
                for(int j=0; j< arrays.get(i).size(); j++){
                    stmt.setString(j+1, arrays.get(i).get(j));
                }
                stmt.addBatch();
            }
            errorQuery = String.valueOf(stmt);
            stmt.executeBatch() ;
            connection.commit();
        }
        catch (SQLException e) {
            System.err.println("Processor for entries: " +String.valueOf(checkpoint) +"-"+String.valueOf(checkpoint+batchsize));
            System.err.println("Could not store data:" + String.valueOf(e));
            System.err.println("Query used:" +errorQuery);
            System.err.println("-----End of Error output-----");
            //throw new IllegalStateException("Cannot connect to the database!", e);
        }
        arrays = null;
    }

    /**
     * Updates the progress of the file in the corresponding mysql table
     * TODO: Use different columns for progress
     * TODO: Change column type for filename
     * TODO: Save cropped filename
     */
    public void updateProgress(){
        double ch = 0.0;
        String selectQuery = null;
        String insertQuery = null;
        String errorSelectQuery= null;
        String errorInsertQuery = null;
        PreparedStatement insertStatement = null;
        PreparedStatement selectStatement = null;
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            connection.setAutoCommit(false);
            System.out.println("Database connected!");
            String fn = filename.replace("\\","\\\\");
            // handle progress update irregularities
            // count number of rows in table and update progress table accordingly
            selectQuery = "select count(*) from "+table;
//            String query2 = "SELECT `progress` from `"+progressTable+"` where `filename`='"+fn+"'";
            selectStatement = connection.prepareStatement(selectQuery);
            errorSelectQuery = String.valueOf(selectStatement);
            ResultSet rs = selectStatement.executeQuery();
            while(rs.next()){
                ch = rs.getInt(1);
            }

            // test file fields: id,first_name,last_name,email,gender,ip_address
            insertQuery = "INSERT INTO `"+progressTable+"`" +
                    "(`id`, `filename`, `progress`)" +
                    "VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE `progress` = VALUES (`progress`);";
            insertStatement = connection.prepareStatement(insertQuery);
            insertStatement.setString(1, String.valueOf(fileID));
            insertStatement.setString(2, filename);
            double t = (double)ch/totalCount;
            insertStatement.setString(3, String.valueOf(t));
            errorInsertQuery = String.valueOf(insertStatement);
            insertStatement.execute() ;
            connection.commit();
            
        } catch (SQLException e) {
            System.err.println("Processor for entries: " +String.valueOf(checkpoint) +"-"+String.valueOf(checkpoint+batchsize));
            System.err.println("Could not update progress");
            System.err.println("SelectQuery: " + errorSelectQuery);
            System.err.println("InsertQuery: " + errorInsertQuery);
            System.err.println(String.valueOf(e));
            System.err.println("-----End of Error output-----");
            //throw new IllegalStateException("Cannot connect the database!", e);
        }
    }
}